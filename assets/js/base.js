// BASE JS
// just some basic javascript added to the project

// navbar - indicate active page
document.addEventListener("DOMContentLoaded", function() {
    if (window.location.pathname == "/") {
        home = document.getElementById("home");
        home.className += " active";
    } else if (window.location.pathname == "/users") {
        users = document.getElementById("users");
        users.className += " active";
    } else if (window.location.pathname == "/sessions/new") {
        current_session = document.getElementById("current_session");
        current_session.className += " active";
    } else if (window.location.pathname == "/cms/pages") {
        cms_pages = document.getElementById("cms_pages");
        cms_pages.className += " active";
    }
});
console.log("baseJS is now executing...");
console.log("URI path is " + window.location.pathname);