defmodule IndieWeb.CMS.PageView do
  use IndieWeb, :view

  alias Indie.CMS

  def author_name(%CMS.Page{author: author}) do
    author.user.name
  end
  
end
