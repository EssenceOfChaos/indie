defmodule IndieWeb.PageController do
  use IndieWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
