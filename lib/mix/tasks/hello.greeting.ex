defmodule Mix.Tasks.Hello.Greeting do
  use Mix.Task

  @shortdoc "Sends a greeting to us from Hello Phoenix"

  @moduledoc """
    This is an example of a custom mix task.

  """

  def run(_args) do
    Mix.Task.run "app.start"
    Mix.shell.info "This custom mix task has access to Repo and other infrastructure but doesn't do anything yet!"
  end

  # We can define other functions as needed here.
end